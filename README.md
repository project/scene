INTRODUCTION
------------

This module integrates the 'Scene.js' library:
  - https://daybrush.com/scenejs/

Scene.js is an JavaScript & CSS timeline-based animation library.


FEATURES
--------

'Scene.js' library is:

  - Cross-browser animations

  - Usage with Javascript

  - Easy to use

  - Responsive

  - Customizable


REQUIREMENTS
------------

Scene module included Scene.js library (no requirement).


INSTALLATION
------------

1. Download 'Scene' module - https://www.drupal.org/project/scene

2. Extract and place it in the root of contributed modules directory i.e.
   /modules/contrib/scene or /modules/scene

3. Now, enable 'Scene' module


USAGE
-----

It’s very simple to use a library, This is really all you need to get going:

const scene = new Scene({
  ".class": {
    0: "left: 0px; top: 0px; transform: translate(0px);",
    1: {
      "left": "100px",
      "top": "0px",
      transform: "translate(50px)",
    },
    2: {
      "left": "200px",
      "top": "100px",
      transform: {
        translate: "100px",
      },
    }
  }
}, {
  selector: true,
  easing: "ease-in-out",
}).play();


How does it Work?
-----------------

1. Enable "Scene" module, Follow INSTALLATION in above.

2. Make scene, Follow USAGE in above.

3. Add scene script to your theme/module js file.

4. Enjoy that.


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt


DEMO
----
https://daybrush.com/scenejs/
